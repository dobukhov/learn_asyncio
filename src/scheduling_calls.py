'''
Created on Jul 23, 2017

@author: ic3ob
'''
import asyncio
import functools


def event_handler(loop, stop=False):
    print('Event handler called')
    if stop:
        print('stopping the loop')
        loop.stop()
        
def print_time(loop):
    print('time: ', loop.time())
    

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        print_time(loop)
        loop.call_later(1, functools.partial(event_handler, loop))
        print('starting event loop')
        print_time(loop)
        loop.call_later(3, functools.partial(event_handler, loop, stop=True))
        print_time(loop)
        loop.run_forever()
    finally:
        print('close event loop')
        loop.close()